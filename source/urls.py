"""
URL configuration for source project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from project.views import ProjectListView
from to_do_list.views import MyDayView, TaskDeleteView, TaskUpdateView, PlansView

# from to_do_list.views import ToDoListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('my_day', MyDayView.as_view(), name='my_day_view'),

    path('delete/<int:pk>', TaskDeleteView.as_view(), name='task_delete'),
    path('update/<int:pk>/', TaskUpdateView.as_view(), name='task_update'),
    path('plans/', PlansView.as_view(), name='plans_view'),

    path('accounts/', include('accounts.urls')),

    path('project/', include('project.urls'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
