from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from accounts.views import login_view, logout_view, register_view, RegistrationView, important_view, \
    important_list_view, UserDetailView, UserUpdateView, PasswordChangeView
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', register_view, name='register'),
    path('important/<int:id>', important_view, name='important'),
    path('user/important/', important_list_view, name='important_list'),
    path('profile/', UserDetailView.as_view(), name='user_detail'),
    path('profile/edit/', UserUpdateView.as_view(), name='user_edit'),
    path('profile/change_password/', PasswordChangeView.as_view(), name='change_password')
    # path('login', LoginView.as_view(), name='login'),
    # path('logout', LogoutView.as_view(), name='logout'),
]
