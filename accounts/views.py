from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse
from django.views.generic import CreateView, DetailView, UpdateView
from django.contrib.auth.models import User

from accounts.forms import RegistrationForm, UserEditForm, ProfileEditForm, PasswordChangeForm
from accounts.models import Profile
from to_do_list.models import Task


# Create your views here.


def login_view(request):
    context = {}
    print(request.GET)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            else:
                print('no next')
            return redirect('my_day_view')
        else:
            context['has_error'] = True
    return render(request, 'accounts/login.html', context)


@login_required
def logout_view(request):
    logout(request)
    next = request.GET.get('next')
    if next:
        return redirect(next)
    return redirect('home')


def register_view(request):
    if request.user.is_authenticated:
        return redirect('my_day_view')
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            Profile.objects.create(user=user)
            login(request, user)
            return redirect('my_day_view')
    else:
        form = RegistrationForm()
    return render(request, 'accounts/register.html', context={'form': form})


class RegistrationView(CreateView):
    model = User
    template_name = 'accounts/register.html'
    form_class = RegistrationForm

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect(self.get_success_url())

    def get_success_url(self):
        next = self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('get')
        return next


@login_required
def important_view(request, id):
    task = get_object_or_404(Task, id=id)
    if task.important.filter(id=request.user.id).exists():
        task.important.remove(request.user)
        task.check_important = False
    else:
        task.important.add(request.user)
        task.check_important = True
    task.save()
    return redirect('important_list')


def important_list_view(request):
    important_list = Task.objects.filter(important=request.user)
    return render(request, 'my_important.html', {'important_list': important_list})


class UserDetailView(DetailView):
    model = get_user_model()
    template_name = 'accounts/detail_view.html'
    context_object_name = 'user_object'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

    def get_object(self, queryset=None):
        return self.request.user


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    template_name = 'accounts/edit.html'
    context_object_name = 'user_object'
    form_class = UserEditForm

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
            # print(kwargs)
        return super().get_context_data(**kwargs)

    def get_profile_form(self):
        profile_form = {'instance': self.object.profile}
        if self.request.method == 'POST':
            profile_form['data'] = self.request.POST
            profile_form['files'] = self.request.FILES
        return ProfileEditForm(**profile_form)

    def get_success_url(self):
        return reverse('user_detail')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()

        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return render(context)

    def get_object(self, queryset=None):
        return self.request.user


class PasswordChangeView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = PasswordChangeForm
    template_name = 'accounts/change_password.html'
    context_object_name = 'user_object'

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('login')
