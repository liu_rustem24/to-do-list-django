from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from to_do_list.models import Task


# from pipenv.project import Project


# Create your models here.

class ProjectRoles(models.Model):
    ROLE_CHOICES = [
        ('Manager', 'Manager'),
        ('Lead', 'Lead'),
        ('Developer', 'Developer'),
    ]

    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='roles')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_roles')
    role = models.CharField(max_length=20, choices=ROLE_CHOICES)

    class Meta:
        unique_together = (('project', 'user'),)

    def __str__(self):
        return f'{self.project} | {self.user} | {self.role}'


class Project(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project', null=True, blank=True)
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name='name of the project')
    description = models.TextField(max_length=500, null=False, blank=False, verbose_name='description of the project')
    created_at = models.DateField(auto_now_add=True)
    members = models.ManyToManyField(User, related_name='projects', verbose_name='members of the project', blank=True)
    managers = models.ManyToManyField(User, related_name='managers', verbose_name='managers of the project', blank=True)
    leads = models.ManyToManyField(User, related_name='leads', verbose_name='leads of the project', blank=True)
    developers = models.ManyToManyField(User, related_name='developers', verbose_name='developers of the project', blank=True)
    # tasks = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='tasks', null=True, blank=True)

    # project_groups = models.ManyToManyField(Group, related_name='projects', verbose_name='project groups', blank=True)

    def __str__(self):
        members_list = ", ".join(str(member) for member in self.members.all())
        return f'{self.name} \n Members: {members_list} \n, {self.managers}, {self.leads}, {self.developers}'
