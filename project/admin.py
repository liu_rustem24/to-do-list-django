from django.contrib import admin
from django.contrib.auth.models import Permission

from project.models import Project

# Register your models here.

admin.site.register(Permission)
admin.site.register(Project)
