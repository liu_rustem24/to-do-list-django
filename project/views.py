from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.models import User
from project.forms import ProjectForm
from project.models import Project, ProjectRoles
from to_do_list.forms import TaskForm
from django.contrib.auth.models import Group, Permission


# from django.contrib.auth.admin import

# Create your views here.


class ProjectListView(ListView):
    model = Project
    form_class = ProjectForm
    template_name = 'project/list.html'
    context_object_name = 'projects'


class ProjectCreateView(CreateView):
    model = Project
    form_class = ProjectForm
    template_name = 'project/create.html'
    context_object_name = 'project'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['managers'] = [self.request.user.pk]
        initial['owner'] = [self.request.user.pk]
        return initial

    def form_valid(self, form):
        project = form.save(commit=False)
        project.owner = self.request.user
        print('form valid works')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.object.pk})


class ProjectDetailView(DetailView, CreateView):
    model = Project
    template_name = 'project/detail.html'
    form_class = TaskForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        task_form = TaskForm()
        project = self.get_object()
        print(project.managers.all())

        tasks = project.task_set.all()

        context['task_form'] = task_form
        context['tasks'] = tasks

        context['is_manager'] = project.managers.filter(id=self.request.user.id).exists()
        context['is_lead'] = project.leads.filter(id=self.request.user.id).exists()
        context['is_dev'] = project.developers.filter(id=self.request.user.id).exists()

        return context

    def get_success_url(self):
        return reverse('project_list')

    def post(self, request, *args, **kwargs):
        project = self.get_object()
        form = self.get_form()

        if form.is_valid():
            return self.form_valid(form, project)
        else:
            return self.form_invalid(form)

    def form_valid(self, form, project):
        task = form.save(commit=False)
        task.project = project
        task.save()
        return HttpResponseRedirect(self.get_success_url())


class ProjectUpdateView(UpdateView):
    model = Project
    form_class = ProjectForm
    template_name = 'project/update.html'
    context_object_name = 'project'

    def get_success_url(self):
        return reverse('project_list')


# @login_required
class ProjectDeleteView(DeleteView):
    model = Project
    template_name = 'project/delete.html'
    context_object_name = 'projects'

    # success_url = reverse_lazy('project_list')

    # def dispatch(self, request, *args, **kwargs):
    #     # if not request.user.is_authenticated or not self.request.user.groups.filter(name='Manager').exists():
    #     return redirect('project_list')

    # def get(self, request, *args, **kwargs):
    #     return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('project_list')
