from django import forms
from project.models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        exclude = ['created_at']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'members' : forms.SelectMultiple(attrs={'class': 'form-control', 'size':5}),
            'managers': forms.SelectMultiple(attrs={'class': 'form-control', 'size': 5}),
            'leads': forms.SelectMultiple(attrs={'class': 'form-control', 'size': 5}),
            'developers' : forms.SelectMultiple(attrs={'class': 'form-control', 'size':5}),
            # 'created_at': forms.DateField(),
        }

