from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import render, reverse, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.http import urlencode
from django.views.generic import TemplateView, CreateView, ListView, DetailView, DeleteView, UpdateView

from to_do_list.helpers.views import CustomView
from to_do_list.models import Task, Plan, PlanTasks
from to_do_list.forms import TaskForm, PlanForm, SearchForm


# Create your views here

# class MyDayView(CustomView):
#     form_class = TaskForm
#     template_name = 'my_day.html'
#
# def get_context_data(self, **kwargs):
#     context = super().get_context_data(**kwargs)
#     context['tasks'] = Task.objects.all()
#     return context


class MyDayView(LoginRequiredMixin, CreateView, ListView):
    model = Task
    template_name = 'my_day.html'
    form_class = TaskForm
    # context_object_name = 'tasks'
    paginate_by = 7
    paginate_orphans = 0
    form = SearchForm
    search_value = None
    # plans = Plan.objects.all()
    plan_tasks = PlanTasks.objects.all()

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        self.form_class = TaskForm
        return super().get(request, *args, **kwargs)

    def get_search_form(self):
        return self.form(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None

    def get_success_url(self):
        return reverse('my_day_view')

    def get_context_data(self, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form_class
        context['searchform'] = self.form
        context['plans'] = Plan.objects.filter(user=self.request.user)
        context['plan_task'] = self.plan_tasks
        context['tasks'] = Task.objects.filter(user=self.request.user)
        # context['']
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context

    def form_valid(self, form):
        task = form.save(commit=False)
        task.user = self.request.user
        task.save()
        plan_id = self.request.POST.get('plan')
        print("creating: ", plan_id)
        if plan_id:
            plan = get_object_or_404(Plan, pk=plan_id)
            PlanTasks.objects.create(plan=plan, task=task)
        return super().form_valid(form)

    def get_queryset(self):
        # queryset = Task.objects.filter(user=self.request.user)
        queryset = Task.objects.all()
        sort_by = self.request.GET.get('sort_by')

        def custom_order(task):
            statuses = ['Forgotten', 'Done', 'In progress', 'Started']
            return statuses.index(task.status) if task.status in statuses else len(statuses)

        if sort_by == 'status':
            queryset = sorted(queryset, key=custom_order)
        elif sort_by == 'status-reverse':
            queryset = sorted(queryset, key=custom_order, reverse=True)

        if sort_by and sort_by != 'status' and sort_by != 'status-reverse':
            queryset = queryset.order_by(sort_by)
        if self.search_value:
            query = Q(title__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset


class TaskDeleteView(LoginRequiredMixin, DeleteView):
    model = Task
    form_class = TaskForm
    context_object_name = 'tasks'
    success_url = reverse_lazy('my_day_view')
    permission_required = ['to_do_list.delete_task']

    def get(self, request, *args, **kwargs):  # if we want to delete without confirming, So in my case I will this one
        return self.delete(request, *args, **kwargs)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = TaskForm
    template_name = 'my_day.html'
    context_object_name = 'task'
    print('updating')

    def get_context_data(self, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['plans'] = Plan.objects.filter(user=self.request.user)
        return context

    def get_success_url(self):
        return reverse('my_day_view')

    def form_valid(self, form):
        task = form.save(commit=False)
        task.save()
        plan_id = self.request.POST.get('plan')
        print("updating: ", plan_id)
        if plan_id:
            plan = get_object_or_404(Plan, pk=plan_id)
            PlanTasks.objects.update_or_create(task=task, defaults={'plan': plan})
        else:
            PlanTasks.objects.filter(task=task).delete()
        return super().form_valid(form)


class PlansView(LoginRequiredMixin, CreateView, ListView):
    model = Plan
    template_name = 'my_plan.html'
    form_class = PlanForm
    context_object_name = 'plans'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['plan_tasks'] = PlanTasks.objects.all()
        return context

    def get_success_url(self):
        return reverse('plans_view')

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.user = self.request.user  # Set the user to the currently logged-in user
        plan.save()
        return super().form_valid(form)

    def get_queryset(self):
        queryset = Plan.objects.filter(user=self.request.user)
        return queryset
