# Generated by Django 5.0.2 on 2024-02-29 13:18

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('to_do_list', '0003_task_important'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='description',
            field=models.TextField(blank=True, max_length=500, null=True, verbose_name='task_description'),
        ),
        migrations.AlterField(
            model_name='task',
            name='plan',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='to_do_list.plan', verbose_name='task_plans'),
        ),
    ]
