from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Plan(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name="plan_title")
    description = models.TextField(null=False, blank=False, verbose_name="plan_description")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="plan_created_at")
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="plans", related_name="plans")
    #
    def __str__(self):
        return f"{self.title}"


class Task(models.Model):
    title = models.CharField(max_length=100, blank=False, null=False, verbose_name='task_title')
    description = models.TextField(max_length=500, blank=True, null=True, verbose_name='task_description')
    status = models.CharField(max_length=100, null=True, blank=True, verbose_name="task_status")
    created_at = models.DateTimeField(auto_now_add=True)
    plan = models.ForeignKey('to_do_list.Plan', null=True, blank=True, on_delete=models.CASCADE, verbose_name="plans",
                             related_name="plans")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks', default=1)
    important = models.ManyToManyField(User, related_name='important', default=None, blank=True)
    check_important = models.BooleanField(default=False)
    project = models.ForeignKey('project.Project', null=True, blank=True, on_delete=models.CASCADE)


    def __str__(self):
        # return f"{self.status}"
        return f"{self.title}"


class PlanTasks(models.Model):
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.plan} - {self.task}"
