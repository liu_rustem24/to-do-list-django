from django import forms
from django.forms import widgets

from to_do_list.models import Plan, Task


class PlanForm(forms.ModelForm):
    class Meta:
        model = Plan
        fields = ['title', 'description']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '...', 'required': True}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': '...', 'required': False}),
        }


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description', 'status', 'plan']

        widgets = {
            'title': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': '+  Add a task', 'required': True,
                       'style': 'border: none; font-weight: lighter;; font-size: 18px'}),
            'description': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': 'description', 'required': False, 'rows': 4}),
            'status': forms.Select(attrs={'class': 'form-select', 'placeholder': 'Status'}),
            'plan': forms.Select(attrs={'class': 'form-control', 'rows': 2, 'required': False})
        }
        labels = {
            'title': 'Title:',
            'description': 'Description:',
            'status': 'status',
            'plan': 'Planned in:'
        }


class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='search')
