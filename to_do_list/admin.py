from django.contrib import admin

from to_do_list.models import Task, Plan


# Register your models here.

class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'status', 'plan', 'created_at')

admin.site.register(Task, TaskAdmin)

class PlanAdmin(admin.ModelAdmin):
    list_display = ('title', 'description')

admin.site.register(Plan, PlanAdmin)
