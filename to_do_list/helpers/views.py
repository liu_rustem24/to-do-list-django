from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.views.generic import View

from to_do_list.models import Task


class ParamMixin():
    form_class = None
    template_name = ''
    model = None
    redirected_url = ''
    context_key = 'objects'
    key_kwarg = 'pk'

    def get_redirect_url(
            self):  # просто геттер для url - ну например с помощью этого мы можем брать 'home_page', 'post_create'-> вот такие url имена мы можем брать
        return self.redirect_url

    def get_object(self):
        pk = self.kwargs[self.key_kwarg]
        return get_object_or_404(self.model, pk=pk)


class CustomView(View, ParamMixin):
    form_class = None
    template_name = ''
    model = None
    redirected_url = ''

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['tasks'] = Task.objects.all()
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        self.object = form.save()
        return redirect(self.get_redirect_url())

    def form_invalid(self, form):
        context = {'form':form}
        return render(self.request, self.template_name, context)

    def get_context_data(self, **kwargs):
        return kwargs

