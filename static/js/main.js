console.log('JS files works 1')

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const months = [
  "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

var now = new Date();
var dayOfWeek = days[now.getDay()] + ", " + months[now.getMonth()] + " " + now.getDate();

if (document.getElementById("datetime")){
  document.getElementById("datetime").innerHTML = dayOfWeek;
} else{
  console.log('there is no datatime')
}


var toggled = false;

function toggleSidebar() {
  toggled = !toggled;
  var sidebar = document.getElementById("sidebar");
  var content = document.getElementById("content");

  if(sidebar || content){
     if (toggled) {
    sidebar.style.left = "0";
    content.style.marginLeft = "300px";
  } else {
    sidebar.style.left = "-300px";
    content.style.marginLeft = "30px";
  }
  } else{
    console.log('there is no sidebar element')
  }

}

document.addEventListener('DOMContentLoaded', function () {
  const titleInput = document.querySelector('#titleInput');
  const addDescription = document.querySelector('#addDescription');

  titleInput.addEventListener('input', function () {
    if (titleInput.value.trim() !== '') {
      addDescription.style.display = 'block';
    } else {
      addDescription.style.display = 'none';
    }
  });
});

document.addEventListener('DOMContentLoaded', function () {
  var descVisible = false;
  const description = document.getElementById('description');
  const addDescriptionBtn = document.getElementById('addDescription');

  addDescriptionBtn.addEventListener('click', function () {
    descVisible = !descVisible;
    if (descVisible) {
      description.style.display = 'block';
      addDescriptionBtn.textContent = 'Hide description';
    } else {
      description.style.display = 'none';
      addDescriptionBtn.textContent = 'Add description';
    }
  });
});

document.addEventListener('DOMContentLoaded', function () {
  var clicked = false;
  const plan = document.getElementById('plan');
  const planbtn = document.getElementById('addPlan');

  planbtn.addEventListener('click', function () {
    clicked = !clicked
    if (clicked) {
      plan.style.display = 'block'
    } else {
      plan.style.display = 'none'
    }
  });
});

document.addEventListener('DOMContentLoaded', function () {
  const taskButtons = document.querySelectorAll('.task-description-btn');

  taskButtons.forEach(function (button) {
    button.addEventListener('click', function () {
      const taskId = this.getAttribute('data-task-id');
      const plan = document.getElementById('task-description--' + taskId);

      if (plan) {
        if (plan.style.display === 'none' || plan.style.display === '') {
          plan.style.display = 'block';
          this.textContent = 'Less info';
        } else {
          plan.style.display = 'none';
          this.textContent = 'More info';
        }
      }
    });
  });
});


document.addEventListener('DOMContentLoaded', function () {
  let checkBox = document.querySelectorAll('.class-task-title');

  checkBox.forEach(function (checkbox) {
    checkbox.addEventListener('change', function () {
      let taskId = this.getAttribute('data-task-id');
      let plan = document.getElementById('task-title-' + taskId);
      let description = document.getElementById('task-description-' + taskId)
      // const status = document.getElementById('task-status-' + taskId)
      let content = document.getElementById('tasks-' + taskId);

      if (this.checked) {
        plan.style.textDecoration = 'line-through';
        description.style.textDecoration = 'line-through';
        // status.style.textDecoration = 'line-through';
        content.style.backgroundColor = '#f5f5f5';

        localStorage.setItem('task-' + taskId, 'checked');
      } else {
        plan.style.textDecoration = 'none';
        description.style.textDecoration = 'none';
        // status.style.textDecoration = 'none';
        content.style.backgroundColor = 'white';

        localStorage.removeItem('task-' + taskId);
      }
    });
  });

});



window.onload = function () {
  document.querySelectorAll('.class-task-title').forEach(checkbox => {
    let taskId = checkbox.getAttribute('data-task-id');
    console.log(localStorage.getItem('task-'+taskId))

    console.log('task-'+taskId)
    let isChecked = localStorage.getItem('task-' + taskId) === 'checked';
    console.log(isChecked, taskId, typeof taskId)
    checkbox.checked = isChecked;
    console.log(checkbox.checked = isChecked)
    if (isChecked) {
      let content = document.getElementById('tasks-' + taskId);
      let plan = document.getElementById('task-title-' + taskId);
      let description = document.getElementById('task-description-' + taskId);
      // const status = document.getElementById('task-status-' + taskId);
      content.style.backgroundColor = '#f5f5f5';
      plan.style.textDecoration = 'line-through';
      description.style.textDecoration = 'line-through';
      // status.style.textDecoration = 'line-through';
    }
  });
  console.log('works edit2')
};

document.addEventListener('DOMContentLoaded', function () {
  const taskButtons = document.querySelectorAll('.edit-btn');

  taskButtons.forEach(function (button) {
    console.log('works edit 1')
    button.addEventListener('click', function () {
      const taskId = this.getAttribute('data-task-id');
      const plan = document.getElementById('edit-task-' + taskId);
      console.log(taskId)
      console.log(plan)

      if (plan) {
        console.log('good')
        if (plan.style.display === 'none' || plan.style.display === '') {
          plan.style.display = 'block';
        } else {
          plan.style.display = 'none';
        }
      }
    });
  });
  console.log('works edit2')
});

console.log('JS files works 2')
